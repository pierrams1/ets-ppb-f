package com.dicoding.picodiploma.myapplication.dashboard

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Card
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import com.dicoding.picodiploma.myapplication.R


@Composable
fun HomeDashboard(navController: NavController) {
    Column(
        modifier = Modifier.fillMaxSize(),
    ) {
        ProfileDashboard()
        Row(
            modifier = Modifier.fillMaxWidth(),
            horizontalArrangement = Arrangement.SpaceBetween
        ){
            MenuItem("Kemajuan Studi")
            MenuItem("Kurikulum")
            MenuItem("Kuesioner IPD")
        }
        Row(modifier = Modifier.fillMaxWidth(), horizontalArrangement = Arrangement.SpaceBetween){
            MenuItem("Kalender Akademik")
            MenuItem("Uang Kuliah")
            MenuItemClickable("Jadwal kuliah", navController)
        }
        Row(modifier = Modifier.fillMaxWidth(), horizontalArrangement = Arrangement.SpaceBetween){
            MenuItem("Transkrip")
        }
    }
}

@Composable
fun MenuItemClickable(menuTitle: String, navController: NavController){
    Card(
        modifier = Modifier
            .size(width = 120.dp, height = 120.dp)
            .padding(6.dp)
            .border(width = 1.dp, color = Color.Gray, shape = RoundedCornerShape(8.dp))
            .clickable {
                navController.navigate("jadwal_kuliah")
            }
    ){
        Text(
            modifier = Modifier
                .fillMaxSize()
                .wrapContentHeight(align = Alignment.Bottom),
            text = "$menuTitle",
            textAlign = TextAlign.Center,
            fontSize = 14.sp
        )
    }
}

@Composable
fun MenuItem(menuTitle: String){
    Card(
        modifier = Modifier
            .size(width = 120.dp, height = 120.dp)
            .padding(6.dp)
            .border(width = 1.dp, color = Color.Gray, shape = RoundedCornerShape(8.dp)),
    ){
        Text(
            modifier = Modifier
                .fillMaxSize()
                .wrapContentHeight(align = Alignment.Bottom),
            text = "$menuTitle",
            textAlign = TextAlign.Center,
            fontSize = 14.sp
        )
    }
}

@Composable
fun ProfileDashboard(){
    Column(
        modifier = Modifier
            .size(height = 200.dp, width = 400.dp)
            .background(
                color = Color(27, 212, 154)
            )
    ){
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .padding(5.dp),
            horizontalArrangement = Arrangement.SpaceBetween
        ){
            MenuDashboard()
        }

        Box(
            modifier = Modifier.fillMaxWidth(),
            contentAlignment = Alignment.Center
        ){
            Image(
                painter = painterResource(id = R.drawable.profile),
                contentDescription = "Profile",
                modifier = Modifier
                    .size(width = 84.dp, height = 84.dp)
                    .clip(CircleShape)
            )
        }

        BioDashboard("PIERRA MUHAMMAD SHOBR", "5025201062 | S-1 Teknik Informatika", "Semester 8 - 2023/2024")
    }
}

@Composable
fun MenuDashboard(){
    Image(
        painter = painterResource(id = R.drawable.telp),
        contentDescription = "Telepon",
        modifier = Modifier.size(width = 32.dp, height = 32.dp)
    )
    Text(text = "myITS")
    Image(
        painter = painterResource(id = R.drawable.hamburger),
        contentDescription = "Menu",
        modifier = Modifier.size(width = 32.dp, height = 32.dp)
    )
}

@Composable
fun BioDashboard(nama: String, nrp: String, semester: String){
    Text(
        modifier = Modifier.fillMaxWidth(),
        text = "$nama",
        textAlign = TextAlign.Center,
        fontSize = 18.sp
    )
    Text(
        modifier = Modifier.fillMaxWidth(),
        text = "$nrp",
        textAlign = TextAlign.Center,
        fontSize = 14.sp
    )
    Text(
        modifier = Modifier.fillMaxWidth(),
        text = "$semester",
        textAlign = TextAlign.Center,
        fontSize = 10.sp
    )
}