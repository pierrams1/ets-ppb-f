package com.dicoding.picodiploma.myapplication.jadwalkuliah

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import com.dicoding.picodiploma.myapplication.R

@Composable
fun JadwalKuliah(navController: NavController){
    Column(modifier = Modifier.fillMaxSize()){
        JadwalKuliahMenu(navController)
        Row(modifier = Modifier.fillMaxWidth().height(80.dp).padding(6.dp)){
            Text(
                text = "SEN",
                fontSize = 24.sp,
                modifier = Modifier
                    .fillMaxHeight()
                    .wrapContentHeight(align = Alignment.CenterVertically)
                    .padding(end = 20.dp)
            )
            Column(modifier = Modifier
                .fillMaxHeight()
                .width(3.dp)
                .background(color = Color(27, 212, 154)),){}
            Column {
                JadwalKuliahItem("Kalkulus - 1", "10:00 - 12:50 | RK-102", "Budi Wiratno S.T, M.Sc")
            }
        }
        Row(modifier = Modifier.fillMaxWidth().height(160.dp).padding(6.dp)){
            Text(
                text = "SEL",
                fontSize = 24.sp,
                modifier = Modifier
                    .fillMaxHeight()
                    .wrapContentHeight(align = Alignment.CenterVertically)
                    .padding(end = 24.dp)
            )
            Column(modifier = Modifier
                .fillMaxHeight()
                .width(3.dp)
                .background(color = Color(27, 212, 154)),){}
            Column(modifier = Modifier.fillMaxHeight()){
                JadwalKuliahItem("Bahasa Indonesia", "07:00 - 9:50 | RK-105", "Cahya Wahyudi S.T, M.Sc")
                JadwalKuliahItem("Dasar Pemrograman", "10:00 - 12:50 | IF-106", "Ratno Budi S.T, M.Sc")
            }
        }
        Row(modifier = Modifier.fillMaxWidth().height(240.dp).padding(6.dp)){
            Text(
                text = "RAB",
                fontSize = 24.sp,
                modifier = Modifier
                    .fillMaxHeight()
                    .wrapContentHeight(align = Alignment.CenterVertically)
                    .padding(end = 19.dp)
            )
            Column(modifier = Modifier
                .fillMaxHeight()
                .width(3.dp)
                .background(color = Color(27, 212, 154)),){}
            Column(modifier = Modifier.fillMaxHeight()){
                JadwalKuliahItem("Kewarganegaraan", "13:00 - 15:00 | RK-102", "Yudianto S.T, M.Sc")
                JadwalKuliahItem("Fisika", "15:30 - 17:30 | RK-104", "Budi Wiratno S.T, M.Sc")
                JadwalKuliahItem("Kimia", "18:00 - 20:00 | RK-101", "Yahya S.T, M.Sc")
            }
        }
    }
}

@Composable
fun JadwalKuliahItem(namaKuliah: String, jadwalKelas: String, namaDosen: String){
    Column(
        modifier = Modifier
            .width(300.dp)
            .height(80.dp)
            .padding(start = 23.dp, end = 3.dp),
        verticalArrangement = Arrangement.Center
    ){
        Text(text = "$namaKuliah")
        Text(text = "$jadwalKelas")
        Text(text = "$namaDosen")
    }
}

@Composable
fun JadwalKuliahMenu(navController: NavController){
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .height(70.dp)
            .background(color = Color(27, 212, 154))
            .padding(6.dp),
        horizontalArrangement = Arrangement.SpaceBetween
    ){
        Image(
            painter = painterResource(id = R.drawable.back),
            contentDescription = "Back",
            modifier = Modifier
                .size(width = 32.dp, height = 32.dp)
                .clickable { navController.navigate("home_dashboard") }
        )
        Column(
            modifier = Modifier.width(300.dp)
        ){
            Row {
                Image(
                    painter = painterResource(id = R.drawable.schedule),
                    contentDescription = "Schedule",
                    modifier = Modifier.size(width = 32.dp, height = 32.dp)
                )
                Text(
                    text = "Jadwal Kelas",
                    modifier = Modifier.padding(start = 5.dp, end = 5.dp),
                    fontSize = 24.sp
                )
            }
        }
    }
}