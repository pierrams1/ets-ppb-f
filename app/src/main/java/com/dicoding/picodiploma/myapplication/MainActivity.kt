package com.dicoding.picodiploma.myapplication

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.runtime.Composable
import androidx.compose.ui.tooling.preview.Preview
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.dicoding.picodiploma.myapplication.dashboard.HomeDashboard
import com.dicoding.picodiploma.myapplication.jadwalkuliah.JadwalKuliah

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            AppScreen()
        }
    }
}

@Preview(showBackground = true)
@Composable
fun AppScreen(){
    val navController = rememberNavController()
    NavHost(navController = navController, startDestination = "home_dashboard") {
        composable("home_dashboard"){
            HomeDashboard(navController)
        }
        composable("jadwal_kuliah"){
            JadwalKuliah(navController)
        }
    }
}